# A WPI git-tips // quick references 

Quick guide of usefull things that I usually forget and need to search online.

### gerrit specific
    - https://gerrit-review.googlesource.com/Documentation/user-upload.html
- push a new branch: (BE AWARE, you need to do this before start adding commits, I forgot about it many many times)
    - git push \<remotenamte> HEAD:refs/heads/\<branchName>
- push a draft change:
    - git push \<remotename> HEAD:refs/drafts/\<branchName> (push the change as a draft, only you and the people you give permission to will see the changes. If after testing you're not happy with it, you can delete the change and there'll be no track of it)
    - git push \<remotename> HEAD:refs/for/\<branchName> (push the change to review)
    - git push \<remotename> HEAD:refs/\<drafts/for>/\<branchName>%r=\<email1>,r=\<email2>,r=\<email3> (automatically set reviewers, so you don't have to do it in the gerrit web)
- get a change:
    - git fetch https://gerrithost/myProject \<refspec> && git checkout/cherry-pick FETCH_HEAD (better to just get this from gerrit change request page directly)

## Commands

### checking history:

- git log
    - -p
    - specify a range:
        - git log SHA1..SHA2 (this will show the commits that *are* in SHA2 but *ARE NOT* in SHA1 -> it'll never show common commits)
        - git log SHA1...SHA2 (this will show commits in SHA1 and SHA2, but it *WON'T* show the common ones)
    - -- \<pathToFile> (commits that change only that file/directory)
        - --all -- \<pathToFile>/\<**/nameOfFile> (search changes in a file in all branches, you can add --diff-filter=[] to indicate what kind of change)
    - --graph
    - --decorate
    - --oneline
    - --author="(author name)" (you can set regex here, simmilar, but not always the same, --commiter. Sometimes the commiter and the author is not the same)
- git diff
    - --stat 
    - --summary 
    - --dirstat (show only the directories with changes)
    - specify a range: git diff SHA1..SHA2
    - --staged (show changes in staged files)
- git show
    - HEAD/tag/SHA
- git branch
    - git branch -a --contains \<SHA/TAG>
- git tag
- git describe (get latest *annotated* tag)
    - --match (regular expression, glob, not very flexible one)
    - --tags (to get annotated and non annotated tags)
    - --abbrev=0 (to get only the tag name, not the number of commits behind or the commit you're in)
- git reflog (check what git commands you did)
    - git reset HEAD@{indexFromReflog} (go back to a state before you did X git commands)

### retrieve changes

- git fetch \<remote name> (update your remotes, it'll not update your branches automatically, but it'll let you know if your local branches are different from the remote ones)
    - --tags (git fetch don't necessarily updates information of new tags unless you specify it)
    - --prune (remote remote references that don't exist anymore)
- git pull (get remote changes into your local branch)
    - --rebase (if you are working in a common branch and someone has push, and submitted in gerrit, some commits, the best way to get those changes is by doing so. This will put your commits on top of the commits already approved and submitted in gerrit)
    - git pull origin \<branch_name> (Careful with this! if you are not in \<branch_name> it'll perform a merge, I avoid this command)
- git rebase
    - -i \<HEAD~N>/\<origen>
- git merge
    - --no-commit (don't create merge commit, allows you to review changes before doing so and to get/discard whatever changes you want. Useful, for example, when getting changes between vsapc_odb and N+1)
    - --no-ff (Sometimes needed in gerrit. Example: From branchA you create branchB and you push, approve and submit some commits, if when you're done you want to merge back branchB into branchA but HEAD in branchA has't changed since you forked, gerrit won't allow you to push those changes, because SHAs are the same and everything is approved. Then you need to do this option to create a merge-commit to be able to push this branchA)
    - --ff (not always possible, equivalent to a rebase)
    - resolve merge conflicts interactive tutorial: https://www.atlassian.com/git/tutorials/using-branches/merge-conflicts (same/similar for rebase, cherry-pick, pull conflicts)
        - --continue/git commit
        - --abort
        - -s (--strategy, "ours", "theirs". When conflicts, ignore the conflicts and get "yours" or "theirs" changes. USE WITH CAUTION. There are other strategies, I've never used any.)
- git cherry-pick \<SHA>
- git revert \<SHA>
- git grep
    - -i (case insensitive)
    - -n (show line number)
    - -l (only filenames, not lines matched)
    - -f (patterns for file)
- git ls-remote (get gerrit refspec from git log in a terminal)
    - git ls-remote | grep \<SHA>
- git checkout \<branchA> \<pathToFile> (Copies a file from branchA to current branch)

### make/save changes

- git add
    - . (add all, tracked and untracked, AVOID, you might have some files that you don't really want to add, if you are doing this check first which files you have tracked and untracked)
    - --patch (interactive tool that allows you to split the changes of a single file so you can create multiple commits)
    - -e (simillar than patch, but, in my opinion, a bit more complicated)
- git commit
    - -a (add tracked, not staged files)
    - -d (deleta a tag, if it's not already in the repo)
- git tag (without arguments, list all existing tags)
    - git tag -a \<tagName-compulsory> \<commitSha-optional> -m "\<message>" (as WoW, in SAPC we're suppossed to create annotated tags, that's done with the `-a` option. If you don't specify a commitSHA it'll attach the tag to the commit you're in, otherwise to the commit indicated. If you don't specify the `-m` option, git will open an editor for you to write a message)
- git stash
    - pop
    - list
- git push (push changes to repository)
    - git push \<remoteName> HEAD:refs/for/\<branchName> (if you're not in \<branchName> you'll push the commits of whatever branch you're in, into the remote branch you indicated in branchName, be careful)
    - git push \<remoteName> \<tagName> (push a single tag indicated by \<tagName> to the repo. DO NOT indicate the flag --tags this flag will push all the tags in your local that are not in the repo)

### branching

- git branch
    - git branch \<branchName> (create new branch, from the current branch you're in)
    - -d (remove local branch that is synced with the remote)
    - -D (remove local branch, even if it's not synced with the remote)
    - -m \<currentName> \<newName> (rename a branch)
- git checkout (move between branches)
    - -b \<newBranchName> (create a new branch, from the current branch you're in, and checkout to it)
    - - (return to previous branch you where in)

### undo changes

- git reset
    - --hard/soft
        - HEAD (if soft, unstaged files, if hard, remove changes)
        - HEAD~N (N: number of commits you want to get rid of)
        - origin/\<branchName> (align with remote branch)
- git checkout
    - \<filename> (discard changes in a tracked, not staged file)
    - . (discard changes in all tracked, not staged files)

### remotes

- git remote
    - -v (show remotes)
    - add \<remoteName> \<remoteURL> (add new remote)
    - rename \<currentName> \<newName>
    
### configuration

- git config
- git alias

### search more info:

- man git \<command>



